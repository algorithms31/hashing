/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.sarocha.hashing;

import java.util.HashMap;
import java.util.Scanner;

/**
 *
 * @author Sarocha
 */
public class Hashing {
    
     public static void HashMap(int arr[]) {
        HashMap<Integer, Integer> hasmap = new HashMap<Integer, Integer>();
 
        for (int i = 0; i < arr.length; i++) {
 
            Integer c = hasmap.get(arr[i]);
 
            if (hasmap.get(arr[i]) == null) {
                hasmap.put(arr[i], 1);
            }else {
                hasmap.put(arr[i], ++c);
            }
        }
        System.out.println(hasmap);
    }
     
    public static void main(String[] args) {
      int arr[] = {27, 12, 35, 91, 41};
      HashMap(arr);
    }
}
